/* eslint-disable react/jsx-filename-extension */

/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

import React from 'react';
import fetch from 'isomorphic-fetch';
import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
} from '@apollo/client';
import {createAuthLink} from 'aws-appsync-auth-link';
import {RetryLink} from '@apollo/client/link/retry';
import {onError} from '@apollo/client/link/error';

// require(`dotenv`).config({
//   path: `.env.${process.env.NODE_ENV}`,
// });

// const {
//   GATSBY_API_URL,
//   GATSBY_API_KEY,
//   AWS_REGION,
//   AWS_AUTHENTICATION_TYPE,
//   NODE_ENV,
// } = process.env;

//TODO: move to environment variables
const GATSBY_API_URL =
  'https://py5sw6ordbgzthu4r5thzrmxte.appsync-api.us-east-1.amazonaws.com/graphql';
const GATSBY_API_KEY = 'da2-b6ufa6jnbzarnhwyjwmuw4bfru';
const AWS_REGION = 'us-east-1';
const AWS_AUTHENTICATION_TYPE = 'API_KEY';

const AppSyncConfig = {
  graphqlEndpoint: GATSBY_API_URL,
  region: AWS_REGION,
  authenticationType: AWS_AUTHENTICATION_TYPE,
  apiKey: GATSBY_API_KEY,
};

const url = AppSyncConfig.graphqlEndpoint;
const region = AppSyncConfig.region;
const auth = {
  type: AppSyncConfig.authenticationType,
  apiKey: AppSyncConfig.apiKey,
};

const retryLink = new RetryLink();

const appLink = new ApolloLink(async (operation, forward) => {
  // const {appName, apiKey} = setupDetailsVar();
  const appName = 'staging',
    apiKey = 1552679244144731;

  const mutationKey = operation.variables.__mutationkey;

  if (mutationKey) {
    const mutationVariables = operation.variables[mutationKey];

    operation.variables[mutationKey] = {
      appName,
      apiKey,
      ...mutationVariables,
    };
  } else {
    operation.variables = {appName, apiKey, ...operation.variables};
  }

  return forward(operation);
});

const errorLink = onError(({graphQLErrors, networkError, operation}) => {
  const mutationKey = operation.variables.__mutationkey;

  if (graphQLErrors) {
    graphQLErrors.forEach((err) => {
      const {message, locations, path} = err;
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
        `Operation error name: ${operation.operationName}`,
      );

      // SentryConfig.captureException(err);
      console.error('ERROR (captured by apollo client): ' + err.message);
    });
  }

  if (networkError && mutationKey) {
    console.tron.log(`[Network error on mutation]: ${networkError}`);

    // hasConnection().then((isConnected) => {
    //   if (!isConnected) {
    //     showNoConnectionAlert();
    //   }
    // });
  }
});

const link = ApolloLink.from([
  errorLink,
  retryLink,
  createAuthLink({url, region, auth}),
  appLink,
  new HttpLink({uri: url, fetch}),
]);

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'cache-and-network',
      errorPolicy: 'all',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    mutate: {
      errorPolicy: 'all',
    },
  },
});

export const wrapRootElement = ({element}) => {
  return <ApolloProvider client={client}>{element}</ApolloProvider>;
};

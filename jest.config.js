module.exports = {
  transform: {
    '^.+\\.jsx?$': `<rootDir>/jest-preprocess.js`,
  },
  moduleNameMapper: {
    '.+\\.(css|styl|less|sass|scss)$': `identity-obj-proxy`,
    '.+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': `<rootDir>/__mocks__/file-mock.js`,
    '^@macanta/constants(.*)$': '<rootDir>/src/constants$1',
    '^@macanta/fixtures(.*)$': '<rootDir>/src/fixtures$1',
    '^@macanta/utils(.*)$': '<rootDir>/src/utils$1',
    '^@macanta/validations(.*)$': '<rootDir>/src/validations$1',
    '^@macanta/images(.*)$': '<rootDir>/src/assets/images$1',
    '^@macanta/graphql(.*)$': '<rootDir>/src/graphql$1',
    '^@macanta/themes(.*)$': '<rootDir>/src/themes$1',
    '^@macanta/hooks(.*)$': '<rootDir>/src/hooks$1',
    '^@macanta/components(.*)$': '<rootDir>/src/components$1',
    '^@macanta/containers(.*)$': '<rootDir>/src/containers$1',
    '^@macanta/modules(.*)$': '<rootDir>/src/modules$1',
    '^@macanta/pages(.*)$': '<rootDir>/src/pages$1',
  },
  testPathIgnorePatterns: [`node_modules`, `\\.cache`, `<rootDir>.*/public`],
  transformIgnorePatterns: [`node_modules/(?!(gatsby)/)`],
  globals: {
    __PATH_PREFIX__: ``,
  },
  testURL: `http://localhost:8000`,
  setupFiles: [`<rootDir>/loadershim.js`],
};

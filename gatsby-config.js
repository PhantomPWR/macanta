require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
});

const {GATSBY_API_URL, GATSBY_API_KEY} = process.env;

module.exports = {
  pathPrefix: '/',
  siteMetadata: {
    title: `Gatsby Material UI Starter`,
    description: `Kick off your next, great Gatsby project with this Material UI starter. This barebones starter ships with the main Gatsby and Material UI configuration files you might need.`,
    author: `@dominicabela`,
  },
  plugins: [
    {
      resolve: `gatsby-alias-imports`,
      options: {
        aliases: {
          '@macanta/constants': `src/constants`,
          '@macanta/fixtures': `src/fixtures`,
          '@macanta/utils': 'src/utils/',
          '@macanta/validations': `src/validations`,
          '@macanta/images': `src/assets/images`,
          '@macanta/graphql': `src/graphql`,
          '@macanta/themes': `src/themes`,
          '@macanta/hooks': `src/hooks`,
          '@macanta/components': `src/components`,
          '@macanta/containers': `src/containers`,
          '@macanta/modules': `src/modules`,
          '@macanta/pages': `src/pages`,
        },
      },
    },
    {
      resolve: `gatsby-source-graphql`,
      options: {
        typeName: `AppSettings`,
        fieldName: `appSettings`,
        url: GATSBY_API_URL,
        headers: {
          'x-api-key': GATSBY_API_KEY,
        },
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    'gatsby-theme-material-ui',
    'gatsby-plugin-emotion',
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    // 'gatsby-theme-apollo',
  ],
};

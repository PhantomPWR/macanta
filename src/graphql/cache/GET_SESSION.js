import {gql} from '@apollo/client';

export default gql`
  query getSession {
    session @client {
      sessionId
    }
  }
`;

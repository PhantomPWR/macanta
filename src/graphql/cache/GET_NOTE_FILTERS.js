import {gql} from '@apollo/client';

export default gql`
  query getNoteFilters {
    noteFilters @client {
      filterBy
      terms
      tags
    }
  }
`;

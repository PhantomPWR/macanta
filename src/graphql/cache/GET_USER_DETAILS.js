import {gql} from '@apollo/client';

export default gql`
  query getUserDetails {
    userDetails @client {
      email
      password
    }
  }
`;

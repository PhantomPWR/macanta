import {gql} from '@apollo/client';

export default gql`
  query getAppSettings($appName: String!, $apiKey: String!) {
    getAppSettings(appName: $appName, apiKey: $apiKey) {
      uiColour
      customLogo {
        imageData
        filename
        type
      }
    }
  }
`;

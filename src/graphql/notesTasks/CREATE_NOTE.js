import {gql} from '@apollo/client';

export default gql`
  mutation createNote($createNoteInput: CreateNoteInput!) {
    createNote(input: $createNoteInput) {
      id
    }
  }
`;

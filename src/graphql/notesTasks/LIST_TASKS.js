import {gql} from '@apollo/client';

export default gql`
  query listTasks(
    $appName: String!
    $apiKey: String!
    $contactId: ID!
    $page: Int
    $limit: Int
    $filter: String
    $filterBy: String
  ) {
    listTasks(
      appName: $appName
      apiKey: $apiKey
      contactId: $contactId
      page: $page
      limit: $limit
      filter: $filter
      filterBy: $filterBy
    ) {
      items {
        id
        accepted
        userId
        creationDate
        contactId
        completionDate
        lastUpdated
        lastUpdatedBy
        endDate
        type
        actionDate
        title
        note
        tags
        assignedBy
        assignedTo
      }
      count
      total
    }
  }
`;

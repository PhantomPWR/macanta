import {gql} from '@apollo/client';

export default gql`
  query listDataObjects($appName: String!, $apiKey: String!) {
    listDataObjects(appName: $appName, apiKey: $apiKey) {
      id
      title
    }
  }
`;

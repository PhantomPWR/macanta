import React, {useState} from 'react';
import PropTypes from 'prop-types';
import MenuItem from '@material-ui/core/MenuItem';

import LoadingIndicator from '@macanta/components/LoadingIndicator';
import SearchIcon from '@material-ui/icons/Search';
import * as Styled from './styles';

const Search = ({
  defaultValue,
  categories,
  suggestions,
  TextFieldProps,
  onSearch,
  loading,
  onChangeCategory,
  onChange,
  getOptionLabel,
  filterOptions,
  ...props
}) => {
  console.log('Search suggestions', suggestions);
  const [value, setValue] = useState(defaultValue);
  const [category, setCategory] = useState('');

  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
    onChangeCategory(event.target.value);
  };

  const handleSearchTextChange = (newVal) => {
    setValue(newVal);
    onChange(newVal);
  };

  const handleSearch = (params) => () => {
    onSearch({
      category: params.category || categories[0]?.value,
      value: params.value,
    });
  };

  return (
    <Styled.Root {...props}>
      {categories.length > 0 && (
        <Styled.SearchSelect
          select
          size="small"
          value={category}
          hiddenLabel
          onChange={handleCategoryChange}
          variant="filled"
          InputProps={{
            disableUnderline: true,
          }}>
          <MenuItem value="" disabled>
            Search by
          </MenuItem>
          {categories.map((cat) => (
            <MenuItem key={cat.value} value={cat.value}>
              {cat.label}
            </MenuItem>
          ))}
        </Styled.SearchSelect>
      )}
      <div
        style={{
          flex: 1,
        }}>
        <Styled.SearchTextField
          search
          size="small"
          value={value}
          hiddenLabel
          variant="outlined"
          onChange={handleSearchTextChange}
          onSelectOption={handleSearch({category, value})}
          placeholder="Search..."
          AutoCompleteProps={{
            options: suggestions,
            getOptionLabel,
            filterOptions,
            style: {
              width: '100%',
            },
          }}
          onEnterPress={handleSearch({category, value})}
          TextFieldProps={TextFieldProps}
          style={{
            width: '100%',
          }}
        />
        {loading && (
          <div
            style={{
              position: 'absolute',
              top: 0,
              right: 80,
              height: '100%',
              display: 'flex',
              alignItems: 'center',
            }}>
            <LoadingIndicator size={20} />
          </div>
        )}
      </div>
      <Styled.ActionButton
        onClick={handleSearch({category, value})}
        variant="contained">
        <SearchIcon />
      </Styled.ActionButton>
    </Styled.Root>
  );
};

Search.defaultProps = {
  defaultValue: '',
  categories: [],
  suggestions: [],
  loading: false,
  onSearch: () => {},
  onChangeCategory: () => {},
  onChange: () => {},
};

Search.propTypes = {
  defaultValue: PropTypes.string,
  categories: PropTypes.arrayOf(
    PropTypes.exact({
      label: PropTypes.string,
      value: PropTypes.string,
    }),
  ),
  loading: PropTypes.bool,
  suggestions: PropTypes.arrayOf(PropTypes.string),
  onSearch: PropTypes.func,
  onChangeCategory: PropTypes.func,
  onChange: PropTypes.func,
};

export default Search;

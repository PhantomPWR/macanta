import {experimentalStyled as styled} from '@material-ui/core/styles';

import TextField from '@macanta/components/TextField';
import MUIButton from '@macanta/components/Button';
import Box from '@material-ui/core/Box';

export const Root = styled(Box)`
  display: flex;
`;

export const SearchSelect = styled(TextField)`
  & > * {
    border-radius: 0;
  }

  ${({theme}) => theme.breakpoints.up('sm')} {
    min-width: 120px;
  }
`;

export const SearchTextField = styled(TextField)`
  width: 100%;
  & > * {
    border-radius: 0;
  }
`;

export const ActionButton = styled(MUIButton)`
  border-radius: 0;
  box-shadow: none;
`;

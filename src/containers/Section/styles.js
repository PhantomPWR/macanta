import {experimentalStyled as styled} from '@material-ui/core/styles';

import PaperComp from '@macanta/components/Paper';
import Box from '@material-ui/core/Box';
import MUIDivider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import LoadingIndicatorComp from '@macanta/components/LoadingIndicator';

export const Root = styled(PaperComp)`
  padding: 0;
  margin: ${({theme}) => theme.spacing(3)} ${({theme}) => theme.spacing(2)} 0;
`;

export const SectionHeader = styled(Box)`
  padding: 0 ${({theme}) => theme.spacing(2)};
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 3.75rem;
  border-bottom: 1px solid ${({theme}) => theme.palette.primary.main};
`;

export const SectionBody = styled(Box)``;

export const Divider = styled(MUIDivider)`
  background-color: ${({theme}) => theme.palette.primary.main};
`;

export const TitleContainer = styled(Box)`
  display: flex;
  align-items: center;
`;

export const LoadingIndicator = styled(LoadingIndicatorComp)`
  margin-left: ${({theme}) => theme.spacing(1)};
`;

export const Title = styled(Typography)``;

export const SectionFooter = styled(Box)`
  padding: 0px ${({theme}) => theme.spacing(2)};
  display: flex;
  align-items: center;
  height: 2.5rem;
  border-top: 1px solid ${({theme}) => theme.palette.primary.main};
`;

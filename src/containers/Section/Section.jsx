import React from 'react';
import * as Styled from './styles';

const Section = ({
  children,
  title,
  HeaderLeftComp,
  HeaderRightComp,
  FooterComp,
  headerStyle,
  bodyStyle,
  loading,
  ...props
}) => {
  return (
    <Styled.Root {...props}>
      <Styled.SectionHeader style={headerStyle}>
        <Styled.TitleContainer>
          {HeaderLeftComp}
          {title && <Styled.Title>{title}</Styled.Title>}
          {loading && <Styled.LoadingIndicator size={14} />}
        </Styled.TitleContainer>
        {HeaderRightComp}
      </Styled.SectionHeader>
      {children && (
        <Styled.SectionBody style={bodyStyle}>{children}</Styled.SectionBody>
      )}
      {FooterComp && <Styled.SectionFooter>{FooterComp}</Styled.SectionFooter>}
    </Styled.Root>
  );
};

export default Section;

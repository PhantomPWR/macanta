import React, {useState} from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import MoreIcon from '@material-ui/icons/MoreVert';
import Drawer from '@macanta/containers/Drawer';
import * as Styled from './styles';

const Header = ({children, renderDesktopMenu, renderMobileMenu, ...props}) => {
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const [mobileSidebarOpen, setMobileSidebarOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileSidebarOpen(!mobileSidebarOpen);
  };

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  return (
    <Styled.Root {...props}>
      <Styled.AppBar position="static">
        <Toolbar>
          <Styled.MenuButton
            onClick={handleDrawerToggle}
            aria-label="Open drawer">
            <MenuIcon />
          </Styled.MenuButton>
          {children}
          <Styled.FlexGrow />
          <Styled.SectionDesktop>{renderDesktopMenu}</Styled.SectionDesktop>
          <Styled.SectionMobile>
            <IconButton aria-haspopup="true" onClick={handleMobileMenuOpen}>
              <MoreIcon />
            </IconButton>
          </Styled.SectionMobile>
        </Toolbar>
      </Styled.AppBar>
      <Drawer
        mobileOpen={mobileSidebarOpen}
        onDrawerToggle={handleDrawerToggle}
      />
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        transformOrigin={{vertical: 'top', horizontal: 'right'}}
        open={isMobileMenuOpen}
        onClose={handleMobileMenuClose}>
        {renderMobileMenu}
      </Menu>
    </Styled.Root>
  );
};

export default Header;

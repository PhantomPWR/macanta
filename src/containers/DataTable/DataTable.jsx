import React from 'react';
import Modal from '@macanta/components/Modal';
import * as Styled from './styles';

const DataTable = ({modal, ModalProps, ...props}) => {
  const table = <Styled.Root {...props} />;
  if (modal) {
    return <Modal {...ModalProps}>{table}</Modal>;
  }
  return table;
};

export default DataTable;

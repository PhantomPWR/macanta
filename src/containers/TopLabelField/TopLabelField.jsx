import React from 'react';
import * as Styled from './styles';

const TopLabelField = ({label, style, ...props}) => {
  return (
    <Styled.Root style={style}>
      {label && <Styled.Label variant="caption">{label}</Styled.Label>}
      <Styled.TextField hiddenLabel {...props} />
    </Styled.Root>
  );
};

export default TopLabelField;

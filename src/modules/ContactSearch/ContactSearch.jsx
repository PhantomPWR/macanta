import React, {useCallback, useState, useEffect, useRef} from 'react';
import {useLazyQuery} from '@apollo/client';
import {LIST_CONTACTS} from '@macanta/graphql/contacts';
import debounce from 'lodash/debounce';
import DataTable from '@macanta/containers/DataTable';
import {createFilterOptions} from '@material-ui/core/Autocomplete';

const filterOptions = createFilterOptions({
  matchFrom: 'any',
  stringify: (option) => JSON.stringify(option),
});

import * as Styled from './styles';

const categories = [
  {
    value: 'name',
    label: 'Name',
  },
  {
    value: 'email',
    label: 'Email',
  },
  {
    value: 'address',
    label: 'Address',
  },
  {
    value: 'company',
    label: 'Company',
  },
  {
    value: 'dataObject',
    label: 'Data Object',
  },
];

const columns = [
  {id: 'name', label: 'Name', minWidth: 170},
  {id: 'company', label: 'Company', minWidth: 170},
  {id: 'email', label: 'Email', minWidth: 170},
  {id: 'city', label: 'City', minWidth: 170},
  {id: 'postalCode', label: 'PostCode', minWidth: 170},
];

const getFullName = (firstName, lastName) => {
  return `${firstName} ${lastName}`.trim();
};

const ContactSearch = ({onSelectContact, ...props}) => {
  const [showModal, setShowModal] = useState(false);
  const [searchResults, setSearchResults] = useState([]);
  const [suggestions, setSuggestions] = useState([]);
  const categoryRef = useRef('name');
  const [callSearchQuery, searchQuery] = useLazyQuery(LIST_CONTACTS);

  const handleSuggestionsSearch = useCallback(
    debounce((value) => {
      console.log('handleSuggestionsSearch', value, categoryRef.current);
      if (value.length > 2) {
        filterContacts({
          category: categoryRef.current,
          search: value,
          page: 0,
          limit: 5,
        });
      } else {
        setSuggestions([]);
      }
    }, 500),
    [],
  );

  const handleChangeCategory = (value) => {
    categoryRef.current = value;
  };

  const handleSearch = ({category, value, limit}) => {
    setShowModal(true);
    filterContacts({search: value, category, page: 0, limit});
  };

  const handleSelectContact = (contact) => {
    setShowModal(false);
    onSelectContact(contact);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const filterContacts = ({search = '', category, page = 0, limit}) => {
    const payload = {page, ...(limit && {limit})};
    switch (category) {
      case 'name': {
        callSearchQuery({variables: {...payload, q: search}});
        break;
      }
      case 'email': {
        callSearchQuery({variables: {...payload, q: `email:${search}`}});
        break;
      }
      case 'company': {
        callSearchQuery({variables: {...payload, q: `company:${search}`}});
        break;
      }
      case 'address': {
        callSearchQuery({variables: {...payload, q: `address:${search}`}});
        break;
      }
      case 'dataObject': {
        callSearchQuery({variables: {...payload, q: search}});
        break;
      }
    }
  };

  useEffect(() => {
    const contacts = searchQuery.data?.listContacts?.items;
    console.log('searched contacts', contacts);
    if (!searchQuery.loading && contacts) {
      setSearchResults(
        contacts.map((item) => ({
          name: getFullName(item.firstName, item.lastName),
          ...item,
        })),
      );

      setSuggestions(contacts);
    }
  }, [searchQuery.loading, searchQuery.data?.listContacts]);

  useEffect(() => {
    if (!searchQuery.loading && searchResults.length === 1 && showModal) {
      handleSelectContact(searchResults[0]);
    }
  }, [searchQuery.loading, searchResults, showModal]);

  return (
    <>
      <Styled.Root
        {...props}
        suggestions={suggestions}
        categories={categories}
        loading={searchQuery.loading}
        onSearch={handleSearch}
        onChangeCategory={handleChangeCategory}
        onChange={handleSuggestionsSearch}
        filterOptions={filterOptions}
        getOptionLabel={(option) =>
          option ? `${option.firstName} ${option.lastName}`.trim() : ''
        }
      />
      <DataTable
        modal
        ModalProps={{
          headerTitle: 'Search Window',
          loading: searchQuery.loading,
          open: showModal,
          onClose: handleCloseModal,
        }}
        columns={columns}
        selectable
        data={searchResults}
        onSelectItem={handleSelectContact}
      />
    </>
  );
};

ContactSearch.defaultProps = {
  onSelectContact: () => {},
};

export default ContactSearch;

import React, {useState, useEffect} from 'react';

import DashboardHeader from './DashboardHeader';
import {experimentalStyled as styled} from '@material-ui/core/styles';
import Grid from '@macanta/components/Grid';
import ContactSearchComp from '@macanta/modules/ContactSearch';
import ContactDetails from '@macanta/modules/ContactDetails';
import SidebarAccess from './SidebarAccess';
import {fade} from '@material-ui/core/styles';
import {useNavigate, useLocation} from '@reach/router';
import Page from '@macanta/components/Page';
import {useLazyQuery} from '@apollo/client';
import {LIST_CONTACTS} from '@macanta/graphql/contacts';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {DEFAULT_ACCESSED_ITEM} from './SidebarAccess/SidebarAccess';

export const ContactSearch = styled(ContactSearchComp)`
  position: relative;
  border-radius: ${({theme}) => theme.shape.borderRadius};
  background-color: ${({theme}) => fade(theme.palette.common.white, 0.15)};
  &:hover {
    background-color: ${({theme}) => fade(theme.palette.common.white, 0.25)};
  }
  margin-right: ${({theme}) => theme.spacing(2)};
  margin-left: 0;
  width: 100%;
  ${({theme}) => theme.breakpoints.up('sm')} {
    margin-left: ${({theme}) => theme.spacing(3)};
    max-width: 428px;
    margin-right: ${({theme}) => theme.spacing(4)};
  }
`;

export const GridAccess = styled(Grid)`
  min-width: 200px;
`;

export const getContactId = (pathname) => pathname.split('/')[3];

export const getSlug = (pathname) => pathname.split('/')[4];

export const getPathnameAfterContactId = (pathname, contactId) =>
  pathname.split(contactId)[1];

const DashboardPage = ({children}) => {
  const navigate = useNavigate();
  const location = useLocation();
  const [callSearchQuery, searchQuery] = useLazyQuery(LIST_CONTACTS);
  const [refreshLoading, setRefreshLoading] = useState(false);
  const [contact, setContact] = useState(null);

  console.log('contact', contact);

  const handleSaveContact = (updatedContact) => {
    console.log('updatedContact', updatedContact);
    handleSelectContact(updatedContact);
  };

  const handleSelectContact = (selectedContact) => {
    setContact(selectedContact);

    const pathname =
      getPathnameAfterContactId(location.pathname, selectedContact.id) ||
      `/${DEFAULT_ACCESSED_ITEM}`;
    let navigationState = {};

    if (getSlug(location.pathname) === 'data-object') {
      navigationState = {
        email: selectedContact.email,
      };
    }

    navigate(`/app/dashboard/${selectedContact.id}${pathname}`, {
      replace: true,
      state: navigationState,
    });
  };

  useEffect(() => {
    const contacts = searchQuery.data?.listContacts?.items;
    if (!searchQuery.loading && contacts) {
      console.log('DASHBOARD CONTACT FROM PARAM', contacts);
      if (contacts[0]) {
        handleSelectContact(contacts[0]);
      } else {
        navigate(`/app/dashboard`, {
          replace: true,
        });
      }

      setRefreshLoading(false);
    }
  }, [searchQuery.loading, searchQuery.data?.listContacts, navigate]);

  useEffect(() => {
    if (!contact && location?.pathname) {
      const contactId = getContactId(location.pathname);
      console.log('pathname contactId', contactId, location.pathname);
      if (contactId) {
        setRefreshLoading(true);
        callSearchQuery({variables: {page: 0, q: contactId}});
      }
    }
  }, []);

  return (
    <Page>
      <DashboardHeader onSaveContact={handleSaveContact}>
        <ContactSearch onSelectContact={handleSelectContact} />
      </DashboardHeader>
      <Grid container>
        {!!contact && (
          <>
            <Grid item xs={12}>
              <ContactDetails
                contact={contact}
                onEditContact={handleSaveContact}
              />
            </Grid>
            <GridAccess item xs={2}>
              <SidebarAccess contactId={contact.id} email={contact.email} />
            </GridAccess>
            <Grid item xs={10}>
              {children}
            </Grid>
          </>
        )}
      </Grid>
      <LoadingIndicator modal loading={refreshLoading} />
    </Page>
  );
};

export default DashboardPage;

import React, {useState} from 'react';
import Section from '@macanta/containers/Section';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {useQuery} from '@apollo/client';
import {LIST_DATA_OBJECTS} from '@macanta/graphql/dataObjects';
import {getSlug} from '@macanta/modules/pages/DashboardPage/DashboardPage';
import ListSubheader from '@material-ui/core/ListSubheader';
import {useNavigate, useLocation} from '@reach/router';

export const DEFAULT_ACCESSED_ITEM = 'notes';

const getAccessedItem = (pathname, item = DEFAULT_ACCESSED_ITEM) => {
  const slug = getSlug(pathname);
  let accessedItem = item;

  if (slug === 'data-object') {
    const groupId = pathname.split('/')[5];
    accessedItem = groupId;
  }

  return accessedItem;
};

const SidebarAccess = ({contactId, email}) => {
  const navigate = useNavigate();
  const location = useLocation();
  const [selectedItem, setSelectedItem] = useState(
    getAccessedItem(location.pathname),
  );
  const doTypesQuery = useQuery(LIST_DATA_OBJECTS, {});
  const listDOTypes = doTypesQuery.data?.listDataObjects;

  const handleAccessLink = (type, doType) => () => {
    if (type === 'notes') {
      setSelectedItem('notes');
      navigate(`/app/dashboard/${contactId}/notes`);
    } else if (type === 'data-object') {
      setSelectedItem(doType.id);
      navigate(
        `/app/dashboard/${contactId}/data-object/${doType.id}/${doType.title}`,
        {
          state: {
            email,
          },
        },
      );
    }
  };

  console.log('listDOTypes', listDOTypes);

  return (
    <Section loading={!listDOTypes} title="Access">
      {listDOTypes && (
        <>
          <List
            style={{
              paddingTop: '8px',
            }}>
            <ListItem
              selected={selectedItem === 'notes'}
              button
              onClick={handleAccessLink('notes')}>
              <ListItemText primary={'Notes'} />
            </ListItem>
            <ListSubheader
              style={{
                paddingLeft: '0.875rem',
                paddingRight: '0.875rem',
                height: '2.3rem',
                lineHeight: '2.75rem',
              }}>
              Data Objects
            </ListSubheader>
            {listDOTypes.map((doType) => {
              return (
                <ListItem
                  key={doType.id}
                  selected={selectedItem === doType.id}
                  button
                  onClick={handleAccessLink('data-object', doType)}>
                  <ListItemText primary={doType.title} />
                </ListItem>
              );
            })}
          </List>
        </>
      )}
    </Section>
  );
};

SidebarAccess.propTypes = {};

export default SidebarAccess;

import React, {useState, useEffect} from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {useLazyQuery} from '@apollo/client';
import {LIST_DATA_OBJECT_ITEMS} from '@macanta/graphql/dataObjects';
import DataTable from '@macanta/containers/DataTable';

const DOTypeListItem = ({item, contactId, email, ...props}) => {
  const [showModal, setShowModal] = useState(false);
  const [searchResults, setSearchResults] = useState([]);
  const [columns, setColumns] = useState([]);

  const [callDOItemsQuery, doItemsQuery] = useLazyQuery(LIST_DATA_OBJECT_ITEMS);

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleAccessLink = () => {
    callDOItemsQuery({
      variables: {
        groupId: item.id,
        contactId,
        email,
      },
    });

    setShowModal(true);
  };

  const handleSelectDOItem = () => {};

  useEffect(() => {
    if (doItemsQuery.data?.listDataObjectItems?.items) {
      const listDOItems = doItemsQuery.data?.listDataObjectItems?.items;
      const fields = doItemsQuery.data?.listDataObjectItems?.fields;

      const selectedFieldLength = fields.filter((field) => field.showInTable)
        .length;
      const fieldLength =
        selectedFieldLength || (fields.length > 3 ? 3 : fields.length);
      const fieldsData = fields.slice(0, fieldLength);
      const dataSliced = listDOItems.slice(0, fieldLength);

      setColumns(
        fieldsData.map((field) => ({
          id: field.name,
          label: field.name,
          minWidth: 170,
        })),
      );
      setSearchResults(
        dataSliced.map(({id, data}) => {
          const flattenedData = data.reduce((acc, d) => {
            acc[d.name] = d.value;
            return acc;
          }, {});

          return {
            ...flattenedData,
            id,
          };
        }),
      );
    }
  }, [doItemsQuery.data?.listDataObjectItems]);

  return (
    <>
      <ListItem button onClick={handleAccessLink} {...props}>
        <ListItemText secondary={item.title} />
      </ListItem>
      <DataTable
        modal
        ModalProps={{
          headerTitle: item.title,
          loading: doItemsQuery.loading,
          open: showModal,
          onClose: handleCloseModal,
        }}
        columns={columns}
        selectable
        data={searchResults}
        onSelectItem={handleSelectDOItem}
      />
    </>
  );
};

DOTypeListItem.propTypes = {};

export default DOTypeListItem;

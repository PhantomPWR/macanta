import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Theme from '@macanta/containers/Theme';
import {GET_APP_SETTINGS} from '@macanta/graphql/admin';
import {useQuery} from '@apollo/client';

const BaseTheme = ({children, ...props}) => {
  const appSettingsQuery = useQuery(GET_APP_SETTINGS, {});

  if (!appSettingsQuery.data?.getAppSettings) {
    return null;
  }

  return (
    <Theme appSettings={appSettingsQuery.data.getAppSettings} {...props}>
      <CssBaseline />
      {children}
    </Theme>
  );
};

export default BaseTheme;

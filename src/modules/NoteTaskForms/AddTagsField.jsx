import React, {useState} from 'react';

import InputAdornment from '@material-ui/core/InputAdornment';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@macanta/components/TextField';
import Button from '@macanta/components/Button';
import Tags from '@macanta/modules/NoteTaskHistory/Tags';
import * as Styled from './styles';

const AddTagsField = ({defaultTags, onChange, ...props}) => {
  const [tags, setTags] = useState(defaultTags);
  const [value, setValue] = useState('');

  const setTagsFn = (updatedTags) => {
    console.log('updatedTags', updatedTags);
    setTags(updatedTags);
    onChange(updatedTags);
  };

  const addTag = () => {
    if (value && !tags.includes(value)) {
      setTagsFn(tags.concat(value));
      setValue('');
    }
  };

  const removeTag = (tagToDelete) => {
    console.log('removeTag', tags, tagToDelete);
    setTagsFn(tags.filter((tag) => tag !== tagToDelete.value));
  };

  const handleTagTextChange = (event) => {
    console.log('handleTagTextChange', event?.target?.value);
    const text = event.target?.value || '';
    const replacedText = text.trim().replace(/\s+/g, '_');
    console.log('replacedText', replacedText);
    setValue(replacedText);
  };

  const handleBlur = () => {
    setTimeout(addTag, 250);
  };

  return (
    <Styled.AddTagsFieldRoot>
      <TextField
        {...props}
        onChange={handleTagTextChange}
        onBlur={handleBlur}
        value={value}
        {...(!!value && {
          InputProps: {
            endAdornment: (
              <InputAdornment position="end">
                <Button onClick={addTag} size="medium" startIcon={<AddIcon />}>
                  Add
                </Button>
              </InputAdornment>
            ),
          },
        })}
      />
      <Styled.AddTagsFieldFooter>
        <Tags
          style={{
            padding: 0,
          }}
          data={tags}
          TagProps={{
            onDelete: removeTag,
          }}
        />
      </Styled.AddTagsFieldFooter>
    </Styled.AddTagsFieldRoot>
  );
};

AddTagsField.defaultProps = {
  defaultTags: [],
  onChange: () => {},
};

export default AddTagsField;

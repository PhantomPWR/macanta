import {useEffect, useState} from 'react';
import {CREATE_TASK, UPDATE_TASK} from '@macanta/graphql/notesTasks';
import {useMutation} from '@apollo/client';

const useTaskMutation = () => {
  const [callCreateTaskMutation, createTaskMutation] = useMutation(
    CREATE_TASK,
    {
      onError() {},
    },
  );
  const [callUpdateTaskMutation, updateTaskMutation] = useMutation(
    UPDATE_TASK,
    {
      onError() {},
    },
  );
  const [data, setData] = useState();
  const loading = Boolean(
    createTaskMutation.loading || updateTaskMutation.loading,
  );
  const error = createTaskMutation.error || updateTaskMutation.error;

  console.log(
    'createTaskMutation.data && updateTaskMutation.data',
    createTaskMutation.data,
    updateTaskMutation.data,
  );
  useEffect(() => {
    const newTask =
      createTaskMutation.data?.createTask ||
      updateTaskMutation.data?.updateTask;
    if (newTask && !loading) {
      setData(newTask);
    }
  }, [
    createTaskMutation.data?.createTask,
    updateTaskMutation.data?.updateTask,
    loading,
  ]);

  return {callCreateTaskMutation, callUpdateTaskMutation, data, loading, error};
};

export default useTaskMutation;

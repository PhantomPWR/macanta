import { useEffect, useState} from 'react';
import {CREATE_NOTE, UPDATE_NOTE} from '@macanta/graphql/notesTasks';
import {useMutation} from '@apollo/client';

const useNoteMutation = () => {
  const [callCreateNoteMutation, createNoteMutation] = useMutation(
    CREATE_NOTE,
    {
      onError() {},
    },
  );
  const [callUpdateNoteMutation, updateNoteMutation] = useMutation(
    UPDATE_NOTE,
    {
      onError() {},
    },
  );
  const [data, setData] = useState();
  const loading = Boolean(
    createNoteMutation.loading || updateNoteMutation.loading,
  );
  const error = createNoteMutation.error || updateNoteMutation.error;

  console.log(
    'createNoteMutation.data && updateNoteMutation.data',
    createNoteMutation.data,
    updateNoteMutation.data,
  );
  useEffect(() => {
    const newNote =
      createNoteMutation.data?.createNote ||
      updateNoteMutation.data?.updateNote;
    if (newNote && !loading) {
      setData(newNote);
    }
  }, [
    createNoteMutation.data?.createNote,
    updateNoteMutation.data?.updateNote,
    loading,
  ]);

  return {callCreateNoteMutation, callUpdateNoteMutation, data, loading, error};
};

export default useNoteMutation;

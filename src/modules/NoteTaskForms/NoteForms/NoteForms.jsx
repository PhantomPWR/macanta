import React, {useEffect} from 'react';
import noteValidationSchema from '@macanta/validations/note';
import Form from '@macanta/components/Form';
import Typography from '@material-ui/core/Typography';
import TurnedInIcon from '@material-ui/icons/TurnedIn';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useNoteMutation from './useNoteMutation';
import * as Styled from '@macanta/modules/NoteTaskForms/styles';

const NoteForms = (props) => {
  const {id, contactId, title = '', note = '', tags = [], onSuccess} = props;

  const isEdit = !!id;

  const initValues = {
    ...(isEdit ? {id} : {contactId}),
    title,
    note,
    tags,
  };

  const {
    callCreateNoteMutation,
    callUpdateNoteMutation,
    data,
    loading,
    error,
  } = useNoteMutation();

  console.log('note initValues', initValues);
  console.log('note data', data, loading, error);

  const handleFormSubmit = (values) => {
    console.log('note handleFormSubmit values', values);

    if (!isEdit) {
      callCreateNoteMutation({
        variables: {
          createNoteInput: values,
          __mutationkey: 'createNoteInput',
        },
      });
    } else {
      callUpdateNoteMutation({
        variables: {
          updateNoteInput: values,
          __mutationkey: 'updateNoteInput',
        },
      });
    }
  };

  useEffect(() => {
    if (data) {
      onSuccess(data);
    }
  }, [data]);

  return (
    <Styled.Root {...props}>
      <Styled.Container>
        <Form
          initialValues={initValues}
          validationSchema={noteValidationSchema}
          onSubmit={handleFormSubmit}>
          {({values, errors, handleChange, handleSubmit, setFieldValue}) => (
            <>
              <Styled.FormGroup>
                <Styled.TextField
                  error={errors.title}
                  onChange={handleChange('title')}
                  label="Title"
                  variant="outlined"
                  value={values.title}
                  fullWidth
                  size="medium"
                />

                <Styled.TextField
                  error={errors.note}
                  onChange={handleChange('note')}
                  label="Description"
                  variant="outlined"
                  value={values.note}
                  fullWidth
                  size="medium"
                  multiline
                  rows={4}
                />

                <Styled.AddTagsField
                  error={errors.tags}
                  onChange={(value) => setFieldValue('tags', value)}
                  label="Tags"
                  variant="outlined"
                  defaultTags={values.tags}
                  fullWidth
                  size="medium"
                />
              </Styled.FormGroup>

              {!!error && (
                <Typography color="error" variant="subtitle2" align="center">
                  {error.message}
                </Typography>
              )}

              <Styled.Footer>
                <Styled.FooterButton
                  variant="contained"
                  startIcon={<TurnedInIcon />}
                  onClick={handleSubmit}>
                  Save
                </Styled.FooterButton>
              </Styled.Footer>
            </>
          )}
        </Form>
      </Styled.Container>
      <LoadingIndicator modal loading={loading} />
    </Styled.Root>
  );
};

NoteForms.propTypes = {};

export default NoteForms;

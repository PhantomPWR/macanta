import React, {useCallback, useState} from 'react';
import Modal from '@macanta/components/Modal';
import NoteForms from '@macanta/modules/NoteTaskForms/NoteForms';
import Button from '@macanta/components/Button';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';

const NoteFormsAddBtn = ({contactId, item, onSuccess, ...props}) => {
  const isEdit = !!item;
  const [showModal, setShowModal] = useState(false);
  const handleShowModal = () => {
    console.log('note default item', item);
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const handleSuccess = useCallback((note) => {
    console.log('onSuccess Note', note, onSuccess);
    onSuccess && onSuccess(note);
    setShowModal(false);
  }, []);

  if (!isEdit) {
    return (
      <>
        <Button
          onClick={handleShowModal}
          size="small"
          variant="contained"
          startIcon={<AddIcon />}
          {...props}>
          Add Note
        </Button>
        <Modal
          headerTitle="Add Note"
          open={showModal}
          onClose={handleCloseModal}>
          <NoteForms contactId={contactId} onSuccess={handleSuccess} />
        </Modal>
      </>
    );
  } else {
    return (
      <>
        <Button onClick={handleShowModal} size="small" startIcon={<EditIcon />}>
          Edit Note
        </Button>
        <Modal
          headerTitle="Add Note"
          open={showModal}
          onClose={handleCloseModal}>
          <NoteForms {...item} onSuccess={handleSuccess} />
        </Modal>
      </>
    );
  }
};

export default NoteFormsAddBtn;

import React from 'react';
import Form from '@macanta/components/Form';
import FieldForms from './FieldForms';
import * as Styled from '@macanta/modules/NoteTaskForms/styles';

const DOItemForms = ({
  // contactId,
  // doId: groupId,
  // doItemId,
  fields,
  data,
  // connectedContacts,
  // attachments,
  // notes,
  // isCreate,
  ...props
}) => {
  // const isEdit = !!id;

  const initValues = {};

  // const {callCreateTaskMutation, data, loading, error} = useDOItemMutation();

  // console.log('doItem data & fields', data, fields);
  // console.log('doItem data', data, loading, error);

  const handleFormSubmit = (values) => {
    console.log('doItem handleFormSubmit values', values);

    // callCreateTaskMutation({
    //   variables: {
    //     createTaskInput: values,
    //     __mutationkey: 'createTaskInput',
    //   },
    // });
  };

  // useEffect(() => {
  //   if (data) {
  //     onSuccess(data);
  //   }
  // }, [data]);

  return (
    <Styled.Root {...props}>
      <Form initialValues={initValues} onSubmit={handleFormSubmit}>
        {() => (
          <>
            <FieldForms fields={fields} data={data} />
          </>
        )}
      </Form>
    </Styled.Root>
  );
};

DOItemForms.propTypes = {};

export default DOItemForms;

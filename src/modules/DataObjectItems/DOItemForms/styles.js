import {experimentalStyled as styled} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

export const Root = styled(Box)`
  padding: ${({theme}) => theme.spacing(2)} 0;
`;

export const SectionsBtnContainer = styled(Box)`
  padding: 0px ${({theme}) => theme.spacing(2)};
`;

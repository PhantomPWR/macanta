import React, {useEffect, useState} from 'react';
import {convertWithSections, transformSectionsData} from './transforms';
import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@macanta/components/Button';
import {SubSection} from '@macanta/containers/Section';
import TopLabelField from '@macanta/containers/TopLabelField';
import * as Styled from './styles';

const getSectionsData = (itemData) => {
  const sectionsObj = convertWithSections(itemData);

  const transformedSectionsData = transformSectionsData(sectionsObj);

  return transformedSectionsData;
};

const getFieldAttrs = (type) => {
  let fieldAttrs = {};

  switch (type) {
    case 'TextArea': {
      fieldAttrs = {
        multiline: true,
        rows: 4,
      };
      break;
    }
  }

  return fieldAttrs;
};

const FieldForms = (props) => {
  const {
    fields,
    data,
    // handleData
  } = props;

  const isEdit = !!data;
  console.log('isEdit', data, fields);
  // const scrollRef = useRef(null);
  // const pickersRef = useRef(null);
  // const requiredFields = useRef(
  //   fields.filter((field) => !!field.required).map((field) => field.name),
  // ).current;
  const [sectionsData, setSectionsData] = useState([]);
  const [sectionNames, setSectionNames] = useState([]);
  const [filteredSectionsData, setFilteredSectionsData] = useState(null);
  // const [updatedDetails, setUpdatedDetails] = useState(null);
  // const [loadingSaveDetails, setLoadingSaveDetails] = useState(false);

  // const copyToClipboardFn = (text) => () => {
  //   copyToClipboard(text);
  // };

  // const handleChange = (type) => (value) => {
  //   console.tron.log('handleChange', type, value);

  //   setUpdatedDetails((state) => ({
  //     ...state,
  //     [type]: value,
  //   }));
  // };

  // const handleSave = async () => {
  //   let requiredFieldsErr = [];

  //   pickersRef.current.closeAll();

  //   requiredFields.forEach((field) => {
  //     const hasValue =
  //       !!updatedDetails[field] ||
  //       (!updatedDetails.hasOwnProperty(field) &&
  //         data.some((item) => item.name === field && !!item.value));

  //     if (!hasValue) {
  //       requiredFieldsErr.push(field);
  //     }
  //   });

  //   if (requiredFieldsErr.length) {
  //     Alert.alert(
  //       `Required fields:${requiredFieldsErr.map((err) => `\n${err}`)}`,
  //     );
  //   } else {
  //     setLoadingSaveDetails(true);
  //     setUpdatedDetails(null);
  //     await handleData(updatedDetails);
  //     setLoadingSaveDetails(false);
  //   }
  // };

  const handleSelectSectionData = (sectionName) => () => {
    const filteredData = sectionsData.find(
      (section) => section.sectionName === sectionName,
    );

    console.log(
      'handleSelectSectionData',
      sectionName,
      sectionsData,
      filteredData,
    );

    setFilteredSectionsData(filteredData);
  };

  useEffect(() => {
    const allSections = getSectionsData(!isEdit ? fields : data);
    const allSectionNames = allSections.map((section) => section.sectionName);

    setSectionsData(allSections);
    setSectionNames(allSectionNames);
    console.log('allSectionNames', allSectionNames);
  }, [fields]);

  useEffect(() => {
    handleSelectSectionData(sectionNames[0])();
  }, [sectionNames]);

  // if (!sectionsData.length) {
  //   return null;
  // }

  // const shouldDisableSave = !updatedDetails;

  console.log('filteredSectionsData', filteredSectionsData, sectionNames);

  return (
    <Styled.Root>
      <Styled.SectionsBtnContainer>
        <ButtonGroup variant="outlined" aria-label="primary button group">
          {sectionNames.map((sectionName, index) => {
            const selected = index === 0;

            return (
              <Button
                {...(selected && {
                  variant: 'contained',
                })}
                key={index}
                onClick={handleSelectSectionData(sectionName)}>
                {sectionName}
              </Button>
            );
          })}
        </ButtonGroup>
      </Styled.SectionsBtnContainer>
      <Grid container>
        {filteredSectionsData &&
          filteredSectionsData.subGroups.map((subGroup, subGroupIndex) => {
            const subGroupName =
              subGroup.subGroupName !== 'subGroupWithoutName'
                ? subGroup.subGroupName
                : '';

            return (
              <Grid key={subGroupIndex} item xs={12} md={3}>
                <SubSection title={subGroupName}>
                  {subGroup.data.map((itemData, subGroupDataIndex) => {
                    const field = fields.find(
                      (f) => f.name === itemData.fieldName,
                    );

                    const fieldAttrs = getFieldAttrs(field?.type);

                    return (
                      <TopLabelField
                        key={subGroupDataIndex}
                        // error={errors.title}
                        // onChange={handleChange('title')}
                        label={itemData.fieldName}
                        variant="outlined"
                        placeholder={field.placeholder}
                        value={itemData.value}
                        // value={values.title}
                        fullWidth
                        size="small"
                        {...fieldAttrs}
                      />
                    );
                  })}
                </SubSection>
              </Grid>
            );
          })}
      </Grid>
      {/* <Select
        defaultValue="All"
        items={sectionNames}
        labelPrefix="Section: "
        listTop={SECTION_FILTER_HEIGHT}
        onSelectPickerItem={handleSelectSectionData}
        style={{
          paddingLeft: moderateScale(12, 0.3),
          paddingRight: Metrics.spaceHorizontal,
          height: SECTION_FILTER_HEIGHT,
          justifyContent: 'flex-end',
        }}
      /> */}
    </Styled.Root>
  );
};

export default FieldForms;

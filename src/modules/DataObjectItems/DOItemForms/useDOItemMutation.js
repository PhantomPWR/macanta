// import {useEffect, useState} from 'react';
// import {CREATE_OR_UPDATE_DO_ITEMS, CONNECT_OR_DISCONNECT_RELATIONSHIP, CREATE_DO_NOTE, UPLOAD_DO_ATTACHMENTS} from '@macanta/graphql/dataObjects';
// import {useMutation} from '@apollo/client';

const useDOItemMutation = () => {
  // const [callCreateDOItemMutation, createDOItemMutation] = useMutation(
  //   CREATE_TASK,
  //   {
  //     onError() {},
  //   },
  // );
  // const [callUpdateDOItemMutation, updateDOItemMutation] = useMutation(
  //   UPDATE_TASK,
  //   {
  //     onError() {},
  //   },
  // );
  // const [data, setData] = useState();
  // const loading = Boolean(
  //   createDOItemMutation.loading || updateDOItemMutation.loading,
  // );
  // const error = createDOItemMutation.error || updateDOItemMutation.error;
  // console.log(
  //   'createDOItemMutation.data && updateDOItemMutation.data',
  //   createDOItemMutation.data,
  //   updateDOItemMutation.data,
  // );
  // useEffect(() => {
  //   const newDOItem =
  //     createDOItemMutation.data?.createDOItem ||
  //     updateDOItemMutation.data?.updateDOItem;
  //   if (newDOItem && !loading) {
  //     setData(newDOItem);
  //   }
  // }, [
  //   createDOItemMutation.data?.createDOItem,
  //   updateDOItemMutation.data?.updateDOItem,
  //   loading,
  // ]);
  // return {callCreateDOItemMutation, callUpdateDOItemMutation, data, loading, error};
};

export default useDOItemMutation;

import React, {useState, useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import Section from '@macanta/containers/Section';
import {useQuery} from '@apollo/client';
import {LIST_DATA_OBJECT_ITEMS} from '@macanta/graphql/dataObjects';
import DataTable from '@macanta/containers/DataTable';
import Button from '@macanta/components/Button';
import AddIcon from '@material-ui/icons/Add';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import DOItemForms from './DOItemForms';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import ListIcon from '@material-ui/icons/List';
import PersonIcon from '@material-ui/icons/Person';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AttachmentIcon from '@material-ui/icons/Attachment';
import * as Styled from './styles';

const DataObjectItems = ({
  contactId,
  groupId,
  doTitle,
  location: {
    state: {email},
  },
}) => {
  const [showForms, setShowForms] = useState(false);
  const [selectedDOItem, setSelectedDOItem] = useState(null);
  const [searchResults, setSearchResults] = useState([]);
  const [columns, setColumns] = useState([]);

  const doItemsQuery = useQuery(LIST_DATA_OBJECT_ITEMS, {
    variables: {
      groupId,
      contactId,
      email,
    },
  });

  const handleAddDOItem = () => {
    setShowForms(true);
  };

  const handleBack = () => {
    setSelectedDOItem(null);
    setShowForms(false);
  };

  const handleEditDOItem = (item) => {
    const doItem = doItemsQuery.data?.listDataObjectItems?.items.find(
      (it) => it.id === item.id,
    );

    setSelectedDOItem(doItem.data);
    setShowForms(true);
  };

  const handleSaveDO = () => {};

  useEffect(() => {
    if (doItemsQuery.data?.listDataObjectItems?.items) {
      const listDOItems = doItemsQuery.data?.listDataObjectItems?.items;
      const fields = doItemsQuery.data?.listDataObjectItems?.fields;

      const selectedFieldLength = fields.filter((field) => field.showInTable)
        .length;
      const fieldLength =
        selectedFieldLength || (fields.length > 3 ? 3 : fields.length);
      const fieldsData = fields.slice(0, fieldLength);
      const dataSliced = listDOItems.slice(0, fieldLength);

      setColumns(
        fieldsData.map((field) => ({
          id: field.name,
          label: field.name,
          minWidth: 170,
        })),
      );
      setSearchResults(
        dataSliced.map(({id, data}) => {
          const flattenedData = data.reduce((acc, d) => {
            acc[d.name] = d.value;
            return acc;
          }, {});

          return {
            ...flattenedData,
            id,
          };
        }),
      );
    }
  }, [doItemsQuery.data?.listDataObjectItems]);

  console.log('selectedDOItem', selectedDOItem);

  return (
    <Grid container>
      <Grid item xs={12}>
        <Section
          loading={doItemsQuery.loading}
          // title={doTitle}
          HeaderLeftComp={
            <Breadcrumbs aria-label="breadcrumb">
              <Styled.TitleCrumbBtn
                disabled
                {...(showForms && {
                  startIcon: (
                    <ArrowBackIcon
                      style={{
                        color: '#333',
                      }}
                    />
                  ),
                  onClick: handleBack,
                  disabled: false,
                })}>
                <Typography color="textPrimary">{doTitle}</Typography>
              </Styled.TitleCrumbBtn>
              {showForms &&
                (!selectedDOItem ? (
                  <Typography>Add Item</Typography>
                ) : (
                  <Typography>Edit Item</Typography>
                ))}
            </Breadcrumbs>
          }
          HeaderRightComp={
            !showForms ? (
              <Button
                onClick={handleAddDOItem}
                size="small"
                variant="contained"
                startIcon={<AddIcon />}>
                Add Item
              </Button>
            ) : (
              <Styled.DOItemNavBtnGroup
                variant="text"
                aria-label="text button group">
                <Styled.DOItemNavBtn
                  startIcon={<ListIcon />}
                  style={{
                    color: '#555',
                    fontWeight: 'bold',
                    fontSize: '0.8rem',
                  }}>
                  Fields
                </Styled.DOItemNavBtn>
                <Styled.DOItemNavBtn
                  startIcon={<PersonIcon />}
                  style={{
                    color: '#bbb',
                  }}>
                  Relationships
                </Styled.DOItemNavBtn>
                <Styled.DOItemNavBtn
                  startIcon={<AssignmentIcon />}
                  style={{
                    color: '#bbb',
                  }}>
                  Notes
                </Styled.DOItemNavBtn>
                <Styled.DOItemNavBtn
                  startIcon={<AttachmentIcon />}
                  style={{
                    color: '#bbb',
                  }}>
                  Attachments
                </Styled.DOItemNavBtn>
              </Styled.DOItemNavBtnGroup>
            )
          }>
          {!showForms ? (
            <DataTable
              columns={columns}
              selectable
              data={searchResults}
              onSelectItem={handleEditDOItem}
            />
          ) : (
            <DOItemForms
              contactId={contactId}
              fields={doItemsQuery.data?.listDataObjectItems?.fields}
              data={selectedDOItem}
              onSuccess={handleSaveDO}
            />
          )}
        </Section>
      </Grid>
    </Grid>
  );
};

DataObjectItems.propTypes = {};

export default DataObjectItems;

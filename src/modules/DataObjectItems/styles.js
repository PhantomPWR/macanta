import {experimentalStyled as styled} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@macanta/components/Button';

export const TitleCrumbBtn = styled(Button)`
  padding: 0;
`;

export const DOItemNavBtnGroup = styled(Box)`
  display: flex;
  align-items: center;
`;

export const DOItemNavBtn = styled(Button)`
  color: #888;
  font-size: 0.75rem;
  margin-left: ${({theme}) => theme.spacing(1)};
`;

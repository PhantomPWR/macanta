import {experimentalStyled as styled} from '@material-ui/core/styles';

import Box from '@material-ui/core/Box';
import MUIDivider from '@material-ui/core/Divider';
import NoteFormsAddBtnComp from '@macanta/modules/NoteTaskForms/NoteFormsAddBtn';
import TaskFormsAddBtnComp from '@macanta/modules/NoteTaskForms/TaskFormsAddBtn';

export const ItemContainer = styled(Box)`
  margin-top: ${({theme}) => theme.spacing(1)};
  background-color: white;
`;

export const Divider = styled(MUIDivider)`
  background-color: #fafafa;
  margin: 0 ${({theme}) => theme.spacing(2)};
`;

export const headerRightButtonStyles = ({theme}) => `
  margin-left: ${theme.spacing(2)}
`;

export const NoteFormsAddBtn = styled(NoteFormsAddBtnComp)`
  ${headerRightButtonStyles}
`;

export const TaskFormsAddBtn = styled(TaskFormsAddBtnComp)`
  ${headerRightButtonStyles}
`;

import React from 'react';
import Typography from '@material-ui/core/Typography';
import * as HistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';
import Tags from '@macanta/modules/NoteTaskHistory/Tags';
import Switch from '@macanta/components/Switch';
import Box from '@material-ui/core/Box';
import moment from 'moment';

const TaskItem = ({item}) => {
  return (
    <Styled.Root>
      <Styled.Header>
        <Box>
          <Typography variant="caption">
            {moment(item.creationDate).format('ddd DD MMM YYYY hh:mmA')}
          </Typography>
          <Styled.Title>{item.title}</Styled.Title>
        </Box>
        <Box>
          <Typography variant="caption">
            Due date: {moment(item.creationDate).format('YYYY-MM-DD')}
          </Typography>
          <Box
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Typography variant="caption">Done</Typography>
            <Switch color="success" />
          </Box>
        </Box>
      </Styled.Header>
      <HistoryStyled.Divider />
      <Styled.Body>
        <Typography>{item.note}</Typography>
        <Styled.AssignedContainer>
          <Typography variant="caption">
            Assigned By: {item.assignedBy}
          </Typography>
          <Typography variant="caption">
            Assigned To: {item.assignedTo}
          </Typography>
        </Styled.AssignedContainer>
      </Styled.Body>
      <HistoryStyled.Divider />
      <Tags data={item.tags} />
    </Styled.Root>
  );
};

TaskItem.propTypes = {};

export default TaskItem;

import {useCallback, useMemo} from 'react';
import {LIST_NOTES, LIST_TASKS} from '@macanta/graphql/notesTasks';
import {useLazyQuery} from '@apollo/client';

const useHistory = () => {
  const [callNoteQuery, noteQuery] = useLazyQuery(LIST_NOTES);
  const [callTaskQuery, taskQuery] = useLazyQuery(LIST_TASKS);
  // const [data, setData] = useState([]);
  const data = useMemo(
    () =>
      [
        ...(noteQuery.data?.listNotes.items || []),
        ...(taskQuery.data?.listTasks.items || []),
      ].sort((item1, item2) => {
        if (item1.creationDate < item2.creationDate) {
          return 1;
        } else if (item1.creationDate > item2.creationDate) {
          return -1;
        }

        return 0;
      }),
    [noteQuery.data?.listNotes, taskQuery.data?.listTasks],
  );

  const loading = Boolean(noteQuery.loading || taskQuery.loading);
  const error = noteQuery.error || taskQuery.error;

  const callHistoryQuery = useCallback(
    ({contactId, page = 0, limit = 10}) => {
      console.log('callHistoryQuery', contactId);
      callNoteQuery({variables: {contactId, page, limit}});
      callTaskQuery({variables: {contactId, page, limit}});
    },
    [callNoteQuery, callTaskQuery],
  );

  return {callHistoryQuery, data, loading, error};
};

export default useHistory;

import React, {useEffect} from 'react';
import Section from '@macanta/containers/Section';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import useHistory from './useHistory';
import NoteItem from './NoteItem';
import TaskItem from './TaskItem';
import * as Styled from './styles';

const NoteTaskHistory = ({contactId}) => {
  const {callHistoryQuery, data, loading} = useHistory();

  console.log('NoteTaskHistory contactId', contactId);

  const handleMutationSuccess = () => {
    callHistoryQuery({contactId, page: 0, limit: 10});
  };

  useEffect(() => {
    callHistoryQuery({contactId, page: 0, limit: 10});
  }, [callHistoryQuery, contactId]);

  return (
    <Grid container>
      <Grid item xs={5}>
        <Section
          headerStyle={{
            backgroundColor: 'white',
          }}
          loading={loading}
          style={{
            backgroundColor: loading ? 'white' : '#f5f6fa',
          }}
          title="History"
          HeaderRightComp={
            <Box>
              <Styled.TaskFormsAddBtn
                contactId={contactId}
                onSuccess={handleMutationSuccess}
              />
              <Styled.NoteFormsAddBtn
                contactId={contactId}
                onSuccess={handleMutationSuccess}
              />
            </Box>
          }>
          {data.map((nt) => {
            const isNote = !nt.assignedTo;
            return (
              <Styled.ItemContainer key={nt.id}>
                {isNote ? (
                  <NoteItem item={nt} onSuccess={handleMutationSuccess} />
                ) : (
                  <TaskItem item={nt} />
                )}
                <Divider
                  style={{
                    backgroundColor: '#fafafa',
                    // height: 2,
                  }}
                />
              </Styled.ItemContainer>
            );
          })}
        </Section>
      </Grid>
    </Grid>
  );
};

NoteTaskHistory.propTypes = {};

export default NoteTaskHistory;

import React from 'react';
import Typography from '@material-ui/core/Typography';
import * as HistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';
import Tags from '@macanta/modules/NoteTaskHistory/Tags';
import Box from '@material-ui/core/Box';
import moment from 'moment';
import NoteFormsAddBtn from '@macanta/modules/NoteTaskForms/NoteFormsAddBtn';

const NoteItem = ({item, onSuccess}) => {
  return (
    <Styled.Root>
      <Styled.Header>
        <Box>
          <Typography variant="caption">
            {moment(item.creationDate).format('ddd DD MMM YYYY hh:mmA')}
          </Typography>
          <Styled.Title>{item.title}</Styled.Title>
        </Box>
        <NoteFormsAddBtn item={item} onSuccess={onSuccess} />
      </Styled.Header>
      <HistoryStyled.Divider />
      <Styled.Body>
        <Typography>{item.note}</Typography>
      </Styled.Body>
      <HistoryStyled.Divider />
      <Tags data={item.tags} />
    </Styled.Root>
  );
};

NoteItem.propTypes = {};

export default NoteItem;

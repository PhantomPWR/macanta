import {experimentalStyled as styled} from '@material-ui/core/styles';

import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

export const Root = styled(Box)`
  padding: 0px;
`;

export const Title = styled(Typography)``;

export const Header = styled(Box)`
  display: flex;
  justify-content: space-between;
  padding: ${({theme}) => theme.spacing(2)};
`;

export const Body = styled(Box)`
  min-height: 5rem;
  padding: ${({theme}) => theme.spacing(2)};
`;

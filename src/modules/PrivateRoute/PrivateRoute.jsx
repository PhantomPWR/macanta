import React from 'react';
import {navigate} from 'gatsby';
import * as Storage from '@macanta/utils/storage';

export const isLoggedIn = () => {
  const session = Storage.getItem('session');
  console.log('session', session);
  return !!session?.sessionId;
};

const PrivateRoute = ({component: Component, location, ...props}) => {
  if (!isLoggedIn() && location.pathname !== `/`) {
    navigate('/');
    return null;
  }

  return <Component {...props} />;
};
export default PrivateRoute;

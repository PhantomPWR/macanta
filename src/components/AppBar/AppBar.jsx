import * as React from 'react';
import * as Styled from './styles';

const AppBar = ({...props}) => <Styled.Root {...props} />;

export default AppBar;

import React, {useState} from 'react';
import TextField from './TextField';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import DatePicker from '@material-ui/lab/DatePicker';
import moment from 'moment';

const convertToDate = (value) => {
  if (!value) {
    return null;
  } else if (typeof value === 'string') {
    return moment(value).toDate();
  }
  return value;
};

const DatePickerField = ({DatePickerProps, value, onChange, ...props}) => {
  const [selectedDate, setSelectedDate] = useState(convertToDate(value));
  const {outputFormat = 'YYYY-MM-DD hh:mmA'} = DatePickerProps;

  const handleDateChange = (newDate) => {
    setSelectedDate(newDate);
    onChange(moment(newDate).format(outputFormat));
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePicker
        value={selectedDate}
        onChange={handleDateChange}
        renderInput={(params) => <TextField {...params} {...props} />}
        {...DatePickerProps}
      />
    </LocalizationProvider>
  );
};

export default DatePickerField;

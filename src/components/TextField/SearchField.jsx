import React, {useState} from 'react';
import TextField from './TextField';
import Autocomplete from '@material-ui/core/Autocomplete';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    display: 'inline-flex',
    width: '100%',
  },
}));

const SearchField = ({
  AutoCompleteProps,
  value,
  onChange,
  onSelectOption,
  ...props
}) => {
  const [selectedSuggestion, setSelectedSuggestion] = useState(value);
  const [inputValue, setInputValue] = useState(value);

  const classes = useStyles();

  const handleSuggestionChange = (e, sValue, reason) => {
    console.log('Autocomplete handleSuggestionChange: ' + sValue, reason);
    setSelectedSuggestion(sValue);

    if (reason === 'select-option') {
      onSelectOption(sValue);
    }
  };

  const handleInputValue = (event, newInputValue) => {
    console.log('handleInputValue: ' + newInputValue);
    setInputValue(newInputValue);
    onChange(newInputValue);
  };

  return (
    <div className={classes.root}>
      <Autocomplete
        disableClearable
        clearOnBlur={false}
        freeSolo
        onChange={handleSuggestionChange}
        value={selectedSuggestion}
        onInputChange={handleInputValue}
        inputValue={inputValue}
        renderInput={(params) => {
          const {inputProps, InputProps, ...paramsProps} = params;

          return (
            <TextField
              {...paramsProps}
              inputProps={{
                ...inputProps,
                className: '',
              }}
              InputProps={{
                ...InputProps,
                className: '',
              }}
              {...props}
            />
          );
        }}
        {...AutoCompleteProps}
      />
    </div>
  );
};

export default SearchField;

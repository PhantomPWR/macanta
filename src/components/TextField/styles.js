import {experimentalStyled as styled} from '@material-ui/core/styles';

import MUITextField from '@material-ui/core/TextField';

export const TextField = styled(MUITextField)`
  min-width: 100px;
`;

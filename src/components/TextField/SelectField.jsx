import React from 'react';

import TextField from './TextField';

const SelectField = (props) => (
  <TextField
    SelectProps={{displayEmpty: !!props.hiddenLabel}}
    {...props}
    select
  />
);

export default SelectField;

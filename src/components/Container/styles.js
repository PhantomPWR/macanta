import {experimentalStyled as styled} from '@material-ui/core/styles';

import MUIContainer from '@material-ui/core/Container';

export const Root = styled(MUIContainer)`
  padding-left: 0px;
  padding-right: 0px;

  ${({theme}) => theme.breakpoints.up('sm')} {
    padding-left: ${({theme}) => theme.spacing(2)}!important;
    padding-right: ${({theme}) => theme.spacing(2)}!important;
  }
`;

import * as React from 'react';
import * as Styled from './styles';

const Container = (props) => <Styled.Root maxWidth="xl" {...props} />;

export default Container;

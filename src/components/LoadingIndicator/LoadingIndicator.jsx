import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(() => ({
  root: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  defaultLoadingContainer: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    '-ms-transform': 'translate(-50%, -50%)',
    transform: 'translate(-50%, -50%)',
  },
  inlineFlex: {
    display: 'inline-flex',
  },
}));

const LoadingIndicator = ({loading, fill, style, ...props}) => {
  const classes = useStyles();

  if (!loading) {
    return null;
  }

  return (
    <div
      className={fill ? classes.defaultLoadingContainer : classes.inlineFlex}
      style={style}>
      <CircularProgress
        style={{
          color: fill ? 'white' : '#aaa',
        }}
        {...props}
      />
    </div>
  );
};

LoadingIndicator.defaultProps = {
  loading: true,
};

LoadingIndicator.propTypes = {
  loading: PropTypes.bool,
};

export default LoadingIndicator;

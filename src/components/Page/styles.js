import {experimentalStyled as styled} from '@material-ui/core/styles';

import Box from '@material-ui/core/Box';

export const Page = styled(Box)`
  height: 100vh;
  padding-bottom: ${({theme}) => theme.spacing(2)};
  overflow-y: scroll;
`;

import React from 'react';

import * as Styled from './styles';

const Page = (props) => <Styled.Page {...props} />;

export default Page;

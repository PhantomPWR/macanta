import React from 'react';
import MUIDrawer from '@material-ui/core/Drawer';

const Drawer = (props) => <MUIDrawer {...props} />;

export default Drawer;

import * as React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import MUITable from '@material-ui/core/Table';
import MUITableBody from '@material-ui/core/TableBody';
import MUITableCell from '@material-ui/core/TableCell';
import MUITableContainer from '@material-ui/core/TableContainer';
import MUITableHead from '@material-ui/core/TableHead';
import MUITablePagination from '@material-ui/core/TablePagination';
import MUITableRow from '@material-ui/core/TableRow';
import MUITableSortLabel from '@material-ui/core/TableSortLabel';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});

function EnhancedTableHead(props) {
  const {columns, order, orderBy, onRequestSort} = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <MUITableHead>
      <MUITableRow>
        {columns.map((column) => (
          <MUITableCell
            key={column.id}
            align={column.align}
            style={{minWidth: column.minWidth || 170}}
            sortDirection={orderBy === column.id ? order : false}>
            <MUITableSortLabel
              active={orderBy === column.id}
              direction={orderBy === column.id ? order : 'asc'}
              onClick={createSortHandler(column.id)}>
              {column.label}
            </MUITableSortLabel>
          </MUITableCell>
        ))}
      </MUITableRow>
    </MUITableHead>
  );
}

EnhancedTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
};

const Table = ({columns, data: rows, selectable, onSelectItem, ...props}) => {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('name');
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleSelectItem = (item) => () => {
    console.log('handleSelectItem', item);
    onSelectItem(item);
  };

  return (
    <Paper className={classes.root} {...props}>
      <MUITableContainer className={classes.container}>
        <MUITable stickyHeader aria-label="sticky table">
          <EnhancedTableHead
            columns={columns}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
          />
          <MUITableBody>
            {stableSort(rows, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item) => {
                return (
                  <MUITableRow
                    hover
                    tabIndex={-1}
                    key={item.code}
                    {...(selectable && {
                      onClick: handleSelectItem(item),
                      style: {
                        cursor: 'pointer',
                      },
                    })}>
                    {columns.map((column) => {
                      const value = item[column.id];
                      return (
                        <MUITableCell key={column.id} align={column.align}>
                          {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value}
                        </MUITableCell>
                      );
                    })}
                  </MUITableRow>
                );
              })}
          </MUITableBody>
        </MUITable>
      </MUITableContainer>
      {rows.length > 0 && (
        <MUITablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      )}
    </Paper>
  );
};

Table.defaultProps = {
  columns: [],
  data: [],
  selectable: false,
  onSelectItem: () => {},
};

Table.propTypes = {
  columns: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string,
      label: PropTypes.string,
      minWidth: PropTypes.number,
    }),
  ),
  data: PropTypes.arrayOf(PropTypes.object),
  selectable: PropTypes.bool,
  onSelectItem: PropTypes.func,
};

export default Table;

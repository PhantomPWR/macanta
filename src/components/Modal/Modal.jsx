import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import * as Styled from './styles';
import {IconButton} from '@macanta/components/Button';
import CloseIcon from '@material-ui/icons/Close';
import LoadingIndicator from '@macanta/components/LoadingIndicator';

const Modal = ({children, headerTitle, loading, onClose, ...props}) => (
  <Styled.Root onClose={onClose} {...props}>
    {loading ? (
      <LoadingIndicator fill />
    ) : (
      <>
        <Styled.Container>
          <Styled.InnerContainer>
            <Styled.ModalHeader>
              <Box>
                {!!headerTitle && (
                  <Typography color="grey">{headerTitle}</Typography>
                )}
              </Box>
              <IconButton aria-haspopup="true" onClick={onClose}>
                <CloseIcon />
              </IconButton>
            </Styled.ModalHeader>
            {children}
          </Styled.InnerContainer>
        </Styled.Container>
      </>
    )}
  </Styled.Root>
);

export default Modal;

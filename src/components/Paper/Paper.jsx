import * as React from 'react';
import * as Styled from './styles';

const Paper = ({...props}) => <Styled.Root {...props} />;

export default Paper;

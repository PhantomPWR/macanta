import {experimentalStyled as styled} from '@material-ui/core/styles';

import MUIPaper from '@material-ui/core/Paper';

export const Root = styled(MUIPaper)`
  padding: ${({theme}) => theme.spacing(2)};
`;

import React from 'react';
import * as Styled from './styles';

const Image = ({uri, ...props}) => <Styled.Root src={uri} {...props} />;

export default Image;

import React from 'react';
import MUISwitch from '@material-ui/core/Switch';

const Switch = (props) => {
  return <MUISwitch {...props} />;
};

export default Switch;

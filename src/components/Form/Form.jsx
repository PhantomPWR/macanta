import React from 'react';

import {Formik} from 'formik';

const Form = (props) => (
  <Formik validateOnBlur={false} validateOnChange={false} {...props} />
);

export default Form;

import React from 'react';

import * as Styled from './styles';

const Button = (props) => <Styled.Button {...props} />;

export default Button;

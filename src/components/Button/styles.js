import {experimentalStyled as styled} from '@material-ui/core/styles';

import MUIButton from '@material-ui/core/Button';

export const Button = styled(MUIButton)`
  text-transform: capitalize;
`;

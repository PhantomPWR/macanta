import * as yup from 'yup';

const noteValidationSchema = yup.object().shape({
  title: yup.string().required('Required'),
  note: yup.string(),
  tags: yup.array(),
});

export default noteValidationSchema;

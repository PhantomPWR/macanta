import * as yup from 'yup';

const schema = yup.object().shape({
  email: yup.string().required('Required').email('Enter a valid email'),
  password: yup.string().required('Required'),
});

export default schema;

/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import {Router} from '@reach/router';
import LoginPage from '@macanta/modules/pages/LoginPage';
import DashboardPage from '@macanta/modules/pages/DashboardPage';
import PrivateRoute from '@macanta/modules/PrivateRoute';
import BaseTheme from '@macanta/modules/BaseTheme';
import NoteTaskHistory from '@macanta/modules/NoteTaskHistory';
import DataObjectItems from '@macanta/modules/DataObjectItems';

const IndexPage = () => {
  return (
    <BaseTheme>
      <Router>
        <PrivateRoute path="/app/dashboard" component={DashboardPage}>
          <NoteTaskHistory path=":contactId/notes" />
          <DataObjectItems path=":contactId/data-object/:groupId/:doTitle" />
        </PrivateRoute>
        <LoginPage path="/" />
      </Router>
    </BaseTheme>
  );
};

export default IndexPage;

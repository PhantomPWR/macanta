/* eslint-disable react/jsx-filename-extension */
import React, {useEffect} from 'react';
import LoginPage from '@macanta/modules/pages/LoginPage';
import DashboardPage from '@macanta/modules/pages/DashboardPage';
import * as Storage from '@macanta/utils/storage';
import BaseTheme from '@macanta/modules/BaseTheme';
import {navigate} from 'gatsby';

export const isLoggedIn = () => {
  const session = Storage.getItem('session');
  console.log('session', session);
  return !!session?.sessionId;
};

const IndexPage = () => {
  useEffect(() => {
    if (isLoggedIn()) {
      navigate('/app/dashboard');
    }
  }, []);

  return (
    <BaseTheme>{!isLoggedIn() ? <LoginPage /> : <DashboardPage />}</BaseTheme>
  );
};

export default IndexPage;

const UNIT_SPACING = 8;

const applicationStyles = {
  spacing: UNIT_SPACING,
  spacingLg: UNIT_SPACING * 2,
  spacingXl: UNIT_SPACING * 3,
};

export default applicationStyles;

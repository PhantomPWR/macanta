export default Object.freeze({
  CACHE_FIRST: 'cache-first',
  CACHE_AND_NETWORK: 'cache-and-network',
  NO_CACHE: 'no-cache',
  NETWORK_ONLY: 'network-only',
});
